/**
 *     FriendActions
 *     Copyright (C) 2020  René Hemm
 *  
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *  
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU Affero General Public License for more details.
 *  
 *      You should have received a copy of the GNU Affero General Public License
 *      along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.yasuswoelfe.FriendActions;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GUI implements CommandExecutor {

	Main plugin;

	public GUI(Main plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		Boolean state = false;

		if (plugin.getConfig().getBoolean("settings.enabled.main") == false) {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.disabled").replace("{THING}", "FriendActions"));
			state = true;
		} else {
			if (plugin.getConfig().getBoolean("settings.enabled.gui") == true) {
				state = fagui(sender);
			} else {
				plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.disabled").replace("{THING}", "fagui"));
				state = true;
			}
		}

		return state;
	}

	private boolean fagui(CommandSender sender) {
		if (sender.hasPermission("friendactions.gui")) {
			if (sender instanceof Player) {
				Player p = (Player) sender;
				createGUI(p);
				p.openInventory(plugin.inv);
			} else {
				plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.gui.onlyplayers"));
			}
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.gui.nopermission"));
		}
		return true;
	}

	private void createGUI(Player p) {
		plugin.inv = p.getServer().createInventory(null, 18, "§1Friend§eActions");

		ItemStack item_friend = new ItemStack(Material.BLUE_STAINED_GLASS_PANE, 1);
		ItemMeta meta_friend = item_friend.getItemMeta();
		meta_friend.setDisplayName("§1Friend");
		item_friend.setItemMeta(meta_friend);
		plugin.inv.setItem(0, item_friend);
		plugin.inv.setItem(1, item_friend);
		plugin.inv.setItem(2, item_friend);
		plugin.inv.setItem(3, item_friend);

		ItemStack item_credits = new ItemStack(Material.PAPER);
		ItemMeta meta_credits = item_credits.getItemMeta();
		meta_credits.setDisplayName("§7by §4Alphawolf_Yasu");
		item_credits.setItemMeta(meta_credits);
		plugin.inv.setItem(4, item_credits);

		ItemStack item_actions = new ItemStack(Material.YELLOW_STAINED_GLASS_PANE, 1);
		ItemMeta meta_actions = item_actions.getItemMeta();
		meta_actions.setDisplayName("§eActions");
		item_actions.setItemMeta(meta_actions);
		plugin.inv.setItem(5, item_actions);
		plugin.inv.setItem(6, item_actions);
		plugin.inv.setItem(7, item_actions);
		plugin.inv.setItem(8, item_actions);

		if (plugin.getConfig().getBoolean("settings.enabled.hug") == true) {
			ItemStack item_hug = new ItemStack(Material.GOLDEN_CARROT);
			ItemMeta meta_hug = item_hug.getItemMeta();
			meta_hug.setDisplayName(plugin.translateString(plugin.getConfig().getString("settings.gui.hug.itemname")));
			meta_hug.setLore(plugin.translateString(plugin.getConfig().getList("settings.gui.hug.itemlore"), plugin.hugs));
			item_hug.setItemMeta(meta_hug);
			plugin.inv.setItem(9, item_hug);
		} else {
			disabledItem(9);
		}

		if (plugin.getConfig().getBoolean("settings.enabled.grouphug") == true) {
			ItemStack item_grouphug = new ItemStack(Material.GOLDEN_APPLE);
			ItemMeta meta_grouphug = item_grouphug.getItemMeta();
			meta_grouphug.setDisplayName(plugin.translateString(plugin.getConfig().getString("settings.gui.grouphug.itemname")));
			meta_grouphug.setLore(plugin.translateString(plugin.getConfig().getList("settings.gui.grouphug.itemlore"), plugin.hugs));
			item_grouphug.setItemMeta(meta_grouphug);
			plugin.inv.setItem(10, item_grouphug);
		} else {
			disabledItem(10);
		}

		if (plugin.getConfig().getBoolean("settings.enabled.kiss") == true) {
			ItemStack item_kiss = new ItemStack(Material.APPLE);
			ItemMeta meta_kiss = item_kiss.getItemMeta();
			meta_kiss.setDisplayName(plugin.translateString(plugin.getConfig().getString("settings.gui.kiss.itemname")));
			meta_kiss.setLore(plugin.translateString(plugin.getConfig().getList("settings.gui.kiss.itemlore"), plugin.kisses));
			item_kiss.setItemMeta(meta_kiss);
			plugin.inv.setItem(11, item_kiss);
		} else {
			disabledItem(11);
		}

		if (plugin.getConfig().getBoolean("settings.enabled.pounce") == true) {
			ItemStack item_pounce = new ItemStack(Material.SLIME_BLOCK);
			ItemMeta meta_pounce = item_pounce.getItemMeta();
			meta_pounce.setDisplayName(plugin.translateString(plugin.getConfig().getString("settings.gui.pounce.itemname")));
			meta_pounce.setLore(plugin.translateString(plugin.getConfig().getList("settings.gui.pounce.itemlore"), plugin.pounces));
			item_pounce.setItemMeta(meta_pounce);
			plugin.inv.setItem(12, item_pounce);
		} else {
			disabledItem(12);
		}

		if (plugin.getConfig().getBoolean("settings.enabled.lick") == true) {
			ItemStack item_lick = new ItemStack(Material.PINK_BANNER);
			ItemMeta meta_lick = item_lick.getItemMeta();
			meta_lick.setDisplayName(plugin.translateString(plugin.getConfig().getString("settings.gui.lick.itemname")));
			meta_lick.setLore(plugin.translateString(plugin.getConfig().getList("settings.gui.lick.itemlore"), plugin.licks));
			item_lick.setItemMeta(meta_lick);
			plugin.inv.setItem(13, item_lick);
		} else {
			disabledItem(13);
		}

		if (plugin.getConfig().getBoolean("settings.enabled.slap") == true) {
			ItemStack item_slap = new ItemStack(Material.ANVIL);
			ItemMeta meta_slap = item_slap.getItemMeta();
			meta_slap.setDisplayName(plugin.translateString(plugin.getConfig().getString("settings.gui.slap.itemname")));
			meta_slap.setLore(plugin.translateString(plugin.getConfig().getList("settings.gui.slap.itemlore"), plugin.slaps));
			item_slap.setItemMeta(meta_slap);
			plugin.inv.setItem(14, item_slap);
		} else {
			disabledItem(14);
		}

		if (plugin.getConfig().getBoolean("settings.enabled.poke") == true) {
			ItemStack item_poke = new ItemStack(Material.BIRCH_TRAPDOOR);
			ItemMeta meta_poke = item_poke.getItemMeta();
			meta_poke.setDisplayName(plugin.translateString(plugin.getConfig().getString("settings.gui.poke.itemname")));
			meta_poke.setLore(plugin.translateString(plugin.getConfig().getList("settings.gui.poke.itemlore"), plugin.pokes));
			item_poke.setItemMeta(meta_poke);
			plugin.inv.setItem(15, item_poke);
		} else {
			disabledItem(15);
		}
		
		if (plugin.getConfig().getBoolean("settings.enabled.tickle") == true) {
			ItemStack item_tickle = new ItemStack(Material.FEATHER);
			ItemMeta meta_tickle = item_tickle.getItemMeta();
			meta_tickle.setDisplayName(plugin.translateString(plugin.getConfig().getString("settings.gui.tickle.itemname")));
			meta_tickle.setLore(plugin.translateString(plugin.getConfig().getList("settings.gui.tickle.itemlore"), plugin.tickles));
			item_tickle.setItemMeta(meta_tickle);
			plugin.inv.setItem(16, item_tickle);
		} else {
			disabledItem(16);
		}
		
		if (plugin.getConfig().getBoolean("settings.enabled.caress") == true) {
			ItemStack item_caress = new ItemStack(Material.LEATHER);
			ItemMeta meta_caress = item_caress.getItemMeta();
			meta_caress.setDisplayName(plugin.translateString(plugin.getConfig().getString("settings.gui.caress.itemname")));
			meta_caress.setLore(plugin.translateString(plugin.getConfig().getList("settings.gui.caress.itemlore"), plugin.caress));
			item_caress.setItemMeta(meta_caress);
			plugin.inv.setItem(17, item_caress);
		} else {
			disabledItem(17);
		}
	}

	private void disabledItem(int place) {
		ItemStack di = new ItemStack(Material.BARRIER);
		ItemMeta dm = di.getItemMeta();
		dm.setDisplayName(plugin.translateString(plugin.getConfig().getString("settings.gui.disabled.itemname")));
		dm.setLore(plugin.translateString(plugin.getConfig().getList("settings.gui.disabled.itemlore"), 0));
		di.setItemMeta(dm);
		plugin.inv.setItem(place, di);
	}

}
